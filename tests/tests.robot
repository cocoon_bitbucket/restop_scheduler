*** Settings ***
Documentation     standard tests
...

# libraries
Library    restop_client.rf_baseplugin.Pilot


#Suite Setup  init suite
Suite Setup    fetch restop interface  ${platform_url}
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

#${platform_url}=  http://localhost:5001/restop/api/v1
${platform_url}=  http://10.179.1.246/restop/api/v1

#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***

########
#init suite
#	# check platform capablities with test capablities requirements
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#
#init pilot
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#	run keyword if   '${pilot_mode}'=='dry'  set pilot dry mode
#
#shutdown pilot
#	close session



############### units and macros

Unit Tv Vod play
	[Arguments] 	${tv}
	[Documentation] 	select a vod film and play teaser

	Open session	${tv}

    serial synch    ${tv}

    stb status  ${tv}
    send key    ${tv}	  key=VOD
    stb_status  ${tv}
    send key    ${tv}	  key=Down  timeout=2


    send key    ${tv}	  key=Right  timeout=2
    send key    ${tv}	  key=Right  timeout=2
    send key    ${tv}	  key=OK

    send key    ${tv}	  key=Down    timeout=2
    send key    ${tv}	  key=Down    timeout=2

    send key    ${tv}	  key=OK
    stb_status  ${tv}


Unit STB BASIC TEST
	[Arguments] 	${tv}
	[Documentation] 	test STB TESTING keywords

	Open session	${tv}


    ${stb_env}=  Stb Get Env  ${tv}
    Log Many  ${stb_env}

    #${lcn_tv1}=  tv_get_lcn  ${tv}  tag=tv1
    #Log  ${lcn_tv1}

    # Programme TV
    ${word}=  Wording Get  ${tv}  tag=deskEpg


    #Stb Zap  ${tv}  channel=tv1

    send_key  ${tv}  key=KEY_CHANNELUP

    send_key  ${tv}  key=KEY_INFO

    stb_send_dump  ${tv}


    ${tv_program}=  page_get_info_from_live_banner  ${tv}
    Log Many  ${tv_program}



Unit first
	[Arguments] 	${tv}
	[Documentation]  hello

	Open session	${tv}

    #serial synch  ${tv}

    serial watch  ${tv}  timeout=5

    scheduler start  ${tv}


    send_key  ${tv}  key=1
    #builtin.sleep  2
    #serial synch  ${tv}
    serial watch   ${tv}  timeout=2


    stb_send_dump  ${tv}
    serial watch   ${tv}   timeout=2
    #builtin.sleep  2

    ${info}=  page_get_info_from_live_banner  ${tv}
    # 'TF1'
    log many  ${info}

    serial watch  ${tv}  timeout=5

    send_key  ${tv}  key=2
    builtin.sleep  2

    stb_send_dump  ${tv}
    serial watch   ${tv}  timeout=2
    #builtin.sleep  2

    ${info}=  page_get_info_from_live_banner  ${tv}
    # 'TF1'
    log many  ${info}


    #serial synch  ${tv}

    ${stats}=  scheduler read  ${tv}
    Log  ${stats}


    serial synch  ${tv}


Unit second
	[Arguments] 	${tv}
	[Documentation]  test stb menu select

	Open session	${tv}

    serial synch  ${tv}


    send_key  ${tv}  key=MENU
    serial watch   ${tv}  timeout=2

    menu_select  ${tv}  path=4/1/1
    serial watch   ${tv}  timeout=2

    send_key  ${tv}  key=OK
    serial watch   ${tv}  timeout=2

    send_key  ${tv}  key=MENU
    send_key  ${tv}  key=MENU

    serial synch  ${tv}





*** Test Cases ***


#Vod Play Teaser
#  [Tags]  stb
#  [template]  Unit Tv Vod play
#  tv

#STB BASIC TEST
#  [Tags]  stb
#  [template]  Unit STB BASIC TEST
#  tv


first
  Unit first  tv

second
  Unit second  tv