overview
========

restop-scheduler run a program multiple times and store each execution instance under a separate repository


install 
=======

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-scheduler.git
    cd restop-scheduler
    python setup.py install
    pip install -r requirements.txt



usage
=====

jobs are configured in scheduler.ini



examples
--------

    cd tests
    sheduler.py robot




     
     
